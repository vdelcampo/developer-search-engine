import Vue from "vue";
import App from "./components/App.vue";

import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "@mdi/font/css/materialdesignicons.css";
import "highlight.js/styles/github.css";


Vue.use(Vuetify);

new Vue({
    vuetify: new Vuetify(),
    render: (h) => h(App),
}).$mount("#app");
