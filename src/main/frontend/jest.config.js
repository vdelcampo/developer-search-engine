const config = {
  verbose: true,
  preset: "@vue/cli-plugin-unit-jest",
  collectCoverage: false,
  moduleNameMapper: {
    "^@[/](.+)": "<rootDir>/src/$1",
    "^components[/](.+)": "<rootDir>/src/components/$1",
    "src/components/([^\\.]*)$": "<rootDir>/src/components/$1.vue",
    "^vue$": "vue/dist/vue.common.js",
  },
  transform: {
    "^.+\\.vue$": "vue-jest",
  },
};
module.exports = config;
