package de.codecentric.dse.loader;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import de.codecentric.dse.model.Employee;
import de.codecentric.dse.model.Language;
import de.codecentric.dse.model.RepoLanguageCount;
import de.codecentric.dse.model.Repository;
import de.codecentric.dse.repository.EmployeeRepository;
import de.codecentric.dse.repository.LanguageRepository;
import de.codecentric.dse.repository.RepoLanguageCountRepository;
import de.codecentric.dse.repository.RepoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Component that creates at start the employee/repo/language data.
 *
 * @author Victor del Campo
 */
@Component
@Slf4j
public class EmployeeLoader {

    @Value("${codecentric.github.api.members}")
    private String membersApi;
    @Value("${codecentric.github.api.users}")
    private String usersApi;
    @Value("${codecentric.github.api.user-repos}")
    private String userReposApi;
    @Value("${codecentric.github.api.repo-languages}")
    private String repoLanguagesApi;

    private final RestTemplate restTemplate;

    private final EmployeeRepository employeeRepository;
    private final LanguageRepository languageRepository;
    private final RepoRepository repoRepository;
    private final RepoLanguageCountRepository repoLanguageCountRepository;
    private final ConfigurableEnvironment env;

    public EmployeeLoader(final RestTemplate restTemplate, final EmployeeRepository employeeRepository, final LanguageRepository languageRepository,
                          final RepoRepository repoRepository, final RepoLanguageCountRepository repoLanguageCountRepository, final ConfigurableEnvironment env) {
        this.restTemplate = restTemplate;
        this.employeeRepository = employeeRepository;
        this.languageRepository = languageRepository;
        this.repoRepository = repoRepository;
        this.repoLanguageCountRepository = repoLanguageCountRepository;
        this.env = env;
    }

    private static final String ERROR_MESSAGE = "Error calling %s. %s";

    private static HttpEntity<String> getAuthHeadersEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + "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");
        return new HttpEntity<>(headers);
    }

    private ResponseEntity<String> makeApiCall(final String url) {
        log.info("Making API call: " + url);
        return restTemplate.exchange(url, HttpMethod.GET, getAuthHeadersEntity(), String.class);
    }

    @PostConstruct
    private void postConstruct() {
        if (!Arrays.asList(env.getActiveProfiles()).contains("test")) {
            this.getData();
        }
    }

    public void getData() {
        final List<Employee> employees = employeeRepository.findAll();
        final List<Repository> repositories = repoRepository.findAll();
        final List<RepoLanguageCount> repoLanguageCounts = repoLanguageCountRepository.findAll();
        if (employees.isEmpty()) {
            this.getAllMembers();
        } else if (repositories.isEmpty()) {
            employees.forEach(this::getUserRepos);
        } else if (repoLanguageCounts.isEmpty()) {
            repositories.forEach(f -> this.getRepoLanguagesCounts(f.getEmployee().getLogin(), f));
        }

    }

    private void getAllMembers() {
        try {
            final ResponseEntity<String> response = makeApiCall(membersApi);
            final ObjectMapper mapper = new ObjectMapper();
            final JsonNode root = mapper.readTree(response.getBody());
            ArrayNode arrayNode = (ArrayNode) root;
            for (int i = 0; i < arrayNode.size(); i++) {
                final JsonNode login = arrayNode.get(i).path("login");
                this.getUserInfo(login.asText());
            }
        } catch (final Exception e) {
            log.error(String.format(ERROR_MESSAGE, membersApi, e.getLocalizedMessage()));
        }
    }


    private void getUserInfo(final String login) {
        final String url = String.format(usersApi, login);
        try {
            final ResponseEntity<String> response = makeApiCall(url);
            final ObjectMapper mapper = new ObjectMapper();
            final JsonNode root = mapper.readTree(response.getBody());
            final Employee employeeDB = this.employeeRepository.findByLogin(login);
            if (employeeDB == null) {
                final Employee employee = new Employee();
                employee.setLogin(root.path("login").asText());
                employee.setUrl(root.path("url").asText());
                employee.setName(root.path("name").asText());
                employee.setLocation(root.path("location").asText());
                employee.setEmail(root.path("email").asText());
                employee.setCreatedGithub(LocalDateTime.parse(root.path("created_at").asText(), DateTimeFormatter.ISO_OFFSET_DATE_TIME));
                employee.setPublicRepos(root.path("public_repos").asInt(0));
                employeeRepository.save(employee);
                employee.setRepos(getUserRepos(employee));
            } else if (employeeDB.getRepos() == null) {
                employeeDB.setRepos(getUserRepos(employeeDB));
                employeeRepository.save(employeeDB);
            }
        } catch (final Exception e) {
            log.error(String.format(ERROR_MESSAGE, url, e.getLocalizedMessage()));
        }
    }


    private List<Repository> getUserRepos(final Employee employee) {
        final String url = String.format(userReposApi, employee.getLogin());
        final List<Repository> repos = new ArrayList<>();
        try {
            final ResponseEntity<String> response = makeApiCall(url);
            final ObjectMapper mapper = new ObjectMapper();
            final JsonNode root = mapper.readTree(response.getBody());
            ArrayNode arrayNode = (ArrayNode) root;
            for (int i = 0; i < arrayNode.size(); i++) {
                final JsonNode node = arrayNode.get(i);
                final String repoName = node.path("name").asText();
                final Repository repositoryDB = repoRepository.findByName(repoName);
                if (repositoryDB == null) {
                    final Repository repository = new Repository();
                    repository.setId(node.path("10153940").asLong());
                    repository.setCreatedGithub(LocalDateTime.parse(node.path("created_at").asText(), DateTimeFormatter.ISO_OFFSET_DATE_TIME));
                    repository.setDescription(node.path("description").asText());
                    repository.setUrl(node.path("url").asText());
                    repository.setName(repoName);
                    repository.setEmployee(employee);
                    repoRepository.save(repository);
                    repos.add(repository);
                    repository.setRepoLanguageCounts(getRepoLanguagesCounts(employee.getLogin(), repository));

                }
            }

        } catch (final Exception e) {
            log.error(String.format(ERROR_MESSAGE, url, e.getLocalizedMessage()));
        }
        return repos;
    }

    private List<RepoLanguageCount> getRepoLanguagesCounts(final String login, final Repository repository) {
        final String url = String.format(repoLanguagesApi, login, repository.getName());
        final List<RepoLanguageCount> repoLanguageCounts = new ArrayList<>();
        try {
            final ResponseEntity<String> response = makeApiCall(url);
            final ObjectMapper mapper = new ObjectMapper();
            final JsonNode root = mapper.readTree(response.getBody());
            final Iterator<Map.Entry<String, JsonNode>> iterator = root.fields();
            while (iterator.hasNext()) {
                final Map.Entry<String, JsonNode> languageCount = iterator.next();
                final String languageName = languageCount.getKey();
                final Long count = languageCount.getValue().asLong();
                final Language languageDB = languageRepository.findByName(languageName);
                final RepoLanguageCount repoLanguageCount = new RepoLanguageCount();
                if (languageDB == null) {
                    final Language language = new Language();
                    language.setName(languageName);
                    languageRepository.save(language);
                    repoLanguageCount.setLanguage(language);
                } else {
                    repoLanguageCount.setLanguage(languageDB);
                }
                repoLanguageCount.setCount(count);
                repoLanguageCount.setRepository(repository);
                repoLanguageCountRepository.save(repoLanguageCount);
            }
        } catch (final Exception e) {
            log.error(String.format(ERROR_MESSAGE, url, e.getLocalizedMessage()));
        }
        return repoLanguageCounts;
    }

}
