package de.codecentric.dse.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Setter(value = AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long dbId;

    @Column(name = "dc", nullable = false)
    private LocalDateTime created;

    @Column(name = "dm", nullable = false)
    private LocalDateTime lastModified;

    @PrePersist
    public void prePersist() {
        this.audit();
    }

    @PreUpdate
    public void preUpdate() {
        this.audit();
    }

    private void audit() {
        if (this.dbId == null) {
            this.created = LocalDateTime.now();
        }
        this.lastModified = LocalDateTime.now();
    }
}
