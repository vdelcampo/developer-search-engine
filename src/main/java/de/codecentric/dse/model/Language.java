package de.codecentric.dse.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;


@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
public class Language extends BaseEntity {

    @Column(nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "language")
    @JsonIgnore
    private List<RepoLanguageCount> repoLanguageCounts;

    public Language() {
    }

}
