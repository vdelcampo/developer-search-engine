package de.codecentric.dse.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;


@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
public class RepoLanguageCount extends BaseEntity {

    @ManyToOne
    private Language language;

    @Column(nullable = false)
    private Long count;

    @ManyToOne(optional = false)
    @JsonIgnore
    private Repository repository;

    public RepoLanguageCount() {
    }

}
