package de.codecentric.dse.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.List;


@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
public class Repository extends BaseEntity {

    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String url;

    @Column(nullable = false, length = 512)
    private String description;

    @Column(nullable = false)
    private LocalDateTime createdGithub;

    @OneToMany(mappedBy = "repository", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RepoLanguageCount> repoLanguageCounts;

    @ManyToOne(optional = false)
    @JsonIgnore
    private Employee employee;

    public Repository() {
    }

}
