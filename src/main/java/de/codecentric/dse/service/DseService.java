package de.codecentric.dse.service;

import de.codecentric.dse.controller.DseController;
import de.codecentric.dse.model.Employee;
import de.codecentric.dse.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * A Service implementation for the {@link DseController}.
 *
 * @author Victor del Campo
 */
@Service
public class DseService {

    private final EmployeeRepository employeeRepository;

    public DseService(final EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    public List<Employee> getEmployeesByLanguage(final String language) {
        return employeeRepository.findByLanguageRanking(language.toLowerCase().strip());
    }
}
