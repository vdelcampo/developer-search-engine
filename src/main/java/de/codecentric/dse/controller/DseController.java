package de.codecentric.dse.controller;

import de.codecentric.dse.model.Employee;
import de.codecentric.dse.service.DseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller for retrieving employees.
 *
 * @author Victor del Campo
 */
@RestController
@RequestMapping(DseController.PATH)
public class DseController {

    /**
     * Base path of the controller.
     */
    public static final String PATH = "/api/dse";

    /**
     * Path for getting all employees.
     */
    public static final String GET_EMPLOYEE = "/employees";

    /**
     * Path for getting employee given programming languages.
     */
    public static final String GET_EMPLOYEE_BY_LANGUAGE = "/employeesByLanguage";

    private final DseService dseService;

    public DseController(final DseService dseService) {
        this.dseService = dseService;
    }

    @GetMapping(GET_EMPLOYEE)
    public ResponseEntity<List<Employee>> getEmployees() {
        return ResponseEntity.ok(this.dseService.getEmployees());
    }

    @GetMapping(GET_EMPLOYEE_BY_LANGUAGE)
    public ResponseEntity<List<Employee>> getEmployeesByLanguage(@RequestParam final String language) {
        return ResponseEntity.ok(this.dseService.getEmployeesByLanguage(language));
    }

}
