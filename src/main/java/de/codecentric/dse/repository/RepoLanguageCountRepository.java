package de.codecentric.dse.repository;

import de.codecentric.dse.model.RepoLanguageCount;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RepoLanguageCountRepository extends CrudRepository<RepoLanguageCount, Long> {


    @Override
    List<RepoLanguageCount> findAll();

}
