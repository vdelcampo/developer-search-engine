package de.codecentric.dse.repository;

import de.codecentric.dse.model.Language;
import org.springframework.data.repository.CrudRepository;

public interface LanguageRepository extends CrudRepository<Language, Long> {

    Language findByName(String name);
}
