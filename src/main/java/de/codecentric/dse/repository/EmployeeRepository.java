package de.codecentric.dse.repository;

import de.codecentric.dse.model.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Employee findByLogin(String login);

    @Override
    List<Employee> findAll();


    @Query("select e from Employee e where e.name = ?1")
    List<Employee> getByName(final String name);

    @Query("select e from Employee e where e.dbId in " +
            "   (select r.employee from Repository r where r.dbId in " +
            "       (select rlc.repository from RepoLanguageCount rlc where rlc.language in " +
            "           (select l.dbId from Language l where lower(trim(l.name)) = ?1)))")
    List<Employee> findByLanguageRanking(final String language);
}
