package de.codecentric.dse.repository;

import de.codecentric.dse.model.Repository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RepoRepository extends CrudRepository<Repository, Long> {

    Repository findByName(String name);

    @Override
    List<Repository> findAll();
}
