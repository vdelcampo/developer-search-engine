package de.codecentric.dse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DseApp {

    public static void main(final String[] args) {
        SpringApplication.run(DseApp.class, args);
    }

}
