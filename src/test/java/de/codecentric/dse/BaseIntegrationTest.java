package de.codecentric.dse;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ContextConfiguration(initializers = {de.codecentric.dse.BaseIntegrationTest.Initializer.class})
@Testcontainers
public abstract class BaseIntegrationTest {

    @SuppressWarnings("rawtypes")
    public static PostgreSQLContainer<?> postgreSQLContainerCheckmodule =
            (PostgreSQLContainer) new PostgreSQLContainer("postgres:13.4")
                    .withDatabaseName("integration-tests-dse-db")
                    .withUsername("integration-tests-dse-user")
                    .withPassword("integration-tests-dse-password")
                    .withReuse(true);

    @BeforeAll
    public static void beforeAll() {
        postgreSQLContainerCheckmodule.start();
    }

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(final ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of("spring.datasource.url=" + postgreSQLContainerCheckmodule.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainerCheckmodule.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainerCheckmodule.getPassword()).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
