package de.codecentric.dse.controller;

import de.codecentric.dse.BaseIntegrationTest;
import de.codecentric.dse.model.Employee;
import de.codecentric.dse.model.Language;
import de.codecentric.dse.model.RepoLanguageCount;
import de.codecentric.dse.model.Repository;
import de.codecentric.dse.repository.EmployeeRepository;
import de.codecentric.dse.repository.LanguageRepository;
import de.codecentric.dse.repository.RepoLanguageCountRepository;
import de.codecentric.dse.repository.RepoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DseControllerTest extends BaseIntegrationTest {

    protected TestRestTemplate restTemplate;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private RepoRepository repoRepository;

    @Autowired
    private RepoLanguageCountRepository repoLanguageCountRepository;

    @Autowired
    private LanguageRepository languageRepository;

    @LocalServerPort
    private int port;

    @BeforeEach
    public void initial() {
        this.restTemplate = new TestRestTemplate();
    }

    public String getTestUrl(final String uri) {
        return "http://localhost:" + this.port + "/api/dse/" + uri;
    }

    @BeforeEach
    public void cleanDataBase() {
        this.repoLanguageCountRepository.deleteAll();
        this.languageRepository.deleteAll();
        this.repoRepository.deleteAll();
        this.employeeRepository.deleteAll();
    }

    @Test
    void shouldGetEmployee() {
        // given default
        final Employee employee = new Employee();
        employee.setName("test");
        employee.setCreatedGithub(LocalDateTime.now());
        employee.setLocation("Berlin");
        employee.setLogin("testlogin");
        employee.setPublicRepos(4);
        employee.setUrl("testurl");
        employeeRepository.save(employee);
        // when
        final ResponseEntity<List<Employee>> result = this.restTemplate.exchange(getTestUrl(DseController.GET_EMPLOYEE), HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Employee>>() {
                });
        // then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        final List<Employee> employees = result.getBody();
        assertThat(employees).isNotNull().hasSize(1);
    }


    @Test
    void shouldGetEmployeeByProgrammingLanguage() {
        // given default
        final Employee employee = new Employee();
        employee.setName("test");
        employee.setCreatedGithub(LocalDateTime.now());
        employee.setLocation("Berlin");
        employee.setLogin("testlogin");
        employee.setPublicRepos(4);
        employee.setUrl("testurl");
        employeeRepository.save(employee);


        final Repository repository = new Repository();
        repository.setCreatedGithub(LocalDateTime.now());
        repository.setDescription("description");
        repository.setId(123123L);
        repository.setName("name");
        repository.setUrl("url");
        repository.setEmployee(employee);
        repoRepository.save(repository);

        final Language language = new Language();
        language.setName("Python");
        languageRepository.save(language);

        final RepoLanguageCount repoLanguageCount = new RepoLanguageCount();
        repoLanguageCount.setCount(12345L);
        repoLanguageCount.setLanguage(language);
        repoLanguageCount.setRepository(repository);
        repoLanguageCountRepository.save(repoLanguageCount);

        // when
        final URI uri = UriComponentsBuilder.fromHttpUrl(getTestUrl(DseController.GET_EMPLOYEE_BY_LANGUAGE)).queryParam("language", "Python").build().toUri();
        final ResponseEntity<List<Employee>> result = this.restTemplate.exchange(uri, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Employee>>() {
                });
        // then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        final List<Employee> employees = result.getBody();
        assertThat(employees).isNotNull().hasSize(1);
    }


}
