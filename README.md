# Developer Search Engine

The developer search engine has been developer as a code challenge for the company Codecentric. It uses the Github API to extract fake information in order for the frontend to display it.

Using the web application you can look for employees given a programming language.

## Prerequisites for development

* JDK 11
* Docker

## Create Docker container for database

For the application to use the database please run the docker-compose.yml file:

`docker-compose up`

## Run application

To start the application just run the executable JAR with:

`java -jar Developer-Search-Engine-1.0-SNAPSHOT.jar`

To see the dashboard open:

`http://localhost:8080/`

## The application

The application will automatically import by start-up all information needed from the Github API.

It offers the following functionalities:
1. Display as a tree view of all employees with their programming languages and the count in brackets of in how many projects that languge was used.
2. A developer search given a programming language. It will display all empoyees ranked by number of lines of code written in that language.




